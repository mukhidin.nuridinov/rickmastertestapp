package com.demo.rickmasters.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.rickmasters.R
import com.demo.rickmasters.data.domain.DataItemUi
import com.demo.rickmasters.usecase.CameraUseCase
import com.demo.rickmasters.usecase.DoorsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

private const val TAB_POSITION_CAMERA = 0
private const val TAB_POSITION_DOOR = 1

class MainViewModel(
    private val androidApplication: Application,
    private val cameraUseCase: CameraUseCase,
    private val doorsUseCase: DoorsUseCase
): ViewModel() {

    private val tabsList: List<String> = listOf(
        androidApplication.getString(R.string.cameras),
        androidApplication.getString(R.string.doors)
    )

    private val _state: MutableStateFlow<MainViewState> = MutableStateFlow(MainViewState(tabs = tabsList))
    val state: StateFlow<MainViewState> get() = _state

    private val selectedTab = MutableStateFlow(0)

    init {
        initInitialData()
        getCameras()
    }

    private fun initInitialData() {
        viewModelScope.launch {
            combine(
                selectedTab
            ) { selectedTab ->
                MainViewState(
                    selectedTabIndex = selectedTab.first(),
                    tabs = tabsList
                )
            }.catch { it.printStackTrace() }
                .collect { _state.value = it }
        }
    }

    fun onTabItemSelected(index: Int) {
        selectedTab.value = index
        when(index) {
            TAB_POSITION_CAMERA -> getCameras()
            TAB_POSITION_DOOR -> getDoors()
        }
    }

    fun onRefreshLocalData() {
        when(selectedTab.value) {
            TAB_POSITION_CAMERA -> getCameras(true)
            TAB_POSITION_DOOR -> getDoors(true)
        }
    }

    private fun getCameras(isRefreshLocalData: Boolean = false) {
        viewModelScope.launch {
            cameraUseCase.invoke(isRefreshLocalData)
                .catch { it.printStackTrace() }
                .collect { _state.value = _state.value.copy(items = it, pullRefreshState = false) }
        }
    }

    private fun getDoors(isRefreshLocalData: Boolean = false) {
        viewModelScope.launch {
            doorsUseCase.invoke(isRefreshLocalData)
                .catch { it.printStackTrace() }
                .collect { _state.value = _state.value.copy(items = it, pullRefreshState = false) }
        }
    }
}

data class MainViewState(
    val selectedTabIndex: Int = 0,
    val tabs: List<String>,
    val items: List<DataItemUi> = listOf(),
    val pullRefreshState: Boolean = false
)
