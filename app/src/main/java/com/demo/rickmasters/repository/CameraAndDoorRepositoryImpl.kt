package com.demo.rickmasters.repository

import com.demo.rickmasters.api.RemoteApi
import com.demo.rickmasters.data.db.DataItemDTO
import com.demo.rickmasters.data.db.toDTO
import io.realm.kotlin.Realm

class CameraAndDoorRepositoryImpl(
    private val remoteApi: RemoteApi,
    private val realm: Realm
): CameraAndDoorRepository {

    override suspend fun getCameras(isRefreshLocalData: Boolean): List<DataItemDTO> {
        val query = "dataType == '${DataItemDTO.DataType.TYPE_CAMERA}'"
        val localCameras = getDataItemsFromDB(realm, query)

        if (localCameras.isNotEmpty() && !isRefreshLocalData) {
            return localCameras
        }
        val response = remoteApi.getCameras()

        realm.writeBlocking {
            query(query = query, clazz = DataItemDTO::class)
                .find()
                .let { delete(it) }
            response.data
                .cameras
                .map { it.toDTO(DataItemDTO.DataType.TYPE_CAMERA) }
                .forEach { copyToRealm(it) }
        }
        return getDataItemsFromDB(realm, query)
    }

    override suspend fun getDoors(isRefreshLocalData: Boolean): List<DataItemDTO> {
        val query = "dataType == '${DataItemDTO.DataType.TYPE_DOOR}'"
        val localDoors = getDataItemsFromDB(realm, query)

        if (localDoors.isNotEmpty() && !isRefreshLocalData) {
            return localDoors
        }

        val response = remoteApi.getDoors()
        realm.writeBlocking {
            query(query = query, clazz = DataItemDTO::class)
                .find()
                .let { delete(it) }
            response.data
                .map { it.toDTO(DataItemDTO.DataType.TYPE_DOOR) }
                .forEach { copyToRealm(it) }
        }
        return getDataItemsFromDB(realm, query)
    }

    private suspend fun getDataItemsFromDB(realm: Realm, query: String): List<DataItemDTO> {
        return realm.query(query = query, clazz = DataItemDTO::class).find()
    }
}