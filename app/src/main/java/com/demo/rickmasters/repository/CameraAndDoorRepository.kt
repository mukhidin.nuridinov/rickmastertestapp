package com.demo.rickmasters.repository

import com.demo.rickmasters.data.db.DataItemDTO

interface CameraAndDoorRepository {

    suspend fun getCameras(isRefreshLocalData: Boolean): List<DataItemDTO>

    suspend fun getDoors(isRefreshLocalData: Boolean): List<DataItemDTO>

}