package com.demo.rickmasters.db

import com.demo.rickmasters.data.db.DataItemDTO
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration

fun provideRealmConfig() = RealmConfiguration.create(schema = setOf(DataItemDTO::class))

fun provideRealm(configuration: RealmConfiguration) = Realm.open(configuration)