package com.demo.rickmasters.di

import com.demo.rickmasters.repository.CameraAndDoorRepository
import com.demo.rickmasters.repository.CameraAndDoorRepositoryImpl
import com.demo.rickmasters.usecase.CameraUseCase
import com.demo.rickmasters.usecase.DoorsUseCase
import org.koin.dsl.module

val dataModule = module {
    single<CameraAndDoorRepository> { CameraAndDoorRepositoryImpl(get(), get()) }
    single { CameraUseCase(get()) }
    single { DoorsUseCase(get()) }
}