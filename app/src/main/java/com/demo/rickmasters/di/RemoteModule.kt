package com.demo.rickmasters.di

import com.demo.rickmasters.api.RemoteApi
import com.demo.rickmasters.api.httpClientAndroid
import org.koin.dsl.module

val remoteModule = module {
    single { RemoteApi(httpClientAndroid) }
}