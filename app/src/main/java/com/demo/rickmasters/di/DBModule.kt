package com.demo.rickmasters.di

import com.demo.rickmasters.db.provideRealm
import com.demo.rickmasters.db.provideRealmConfig
import org.koin.dsl.module

val dbModule = module {
    single { provideRealmConfig() }
    single { provideRealm(get()) }
}