package com.demo.rickmasters.di

val appModule = listOf(
    dbModule,
    remoteModule,
    dataModule,
    viewModelModule
)