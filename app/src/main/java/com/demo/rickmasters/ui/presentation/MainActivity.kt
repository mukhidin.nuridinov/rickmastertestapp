package com.demo.rickmasters.ui.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.pullrefresh.PullRefreshIndicator
import androidx.compose.material3.pullrefresh.pullRefresh
import androidx.compose.material3.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.demo.rickmasters.R
import com.demo.rickmasters.ui.theme.RickMastersTheme
import com.demo.rickmasters.viewmodel.MainViewModel
import com.demo.rickmasters.viewmodel.MainViewState
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { initialPage() }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun initialPage(viewModel: MainViewModel = koinViewModel()) {
    RickMastersTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background,

        ) {
            val viewState by viewModel.state.collectAsStateWithLifecycle()
            Column() {
                CenterAlignedTopAppBar(
                    title = {
                        Text(
                            text = stringResource(id = R.string.my_house),
                            fontSize = 18.sp,
                            color = Color.Black
                        )
                    }
                )
                TabBar(
                    tabs = viewState.tabs,
                    onTabItemClick = viewModel::onTabItemSelected,
                    selectedTabIndex = viewState.selectedTabIndex
                )
                CamerasAndDoors(
                    viewState = viewState,
                    viewModel = viewModel
                )
            }
        }
    }
}

@Composable
fun TabBar(
    selectedTabIndex: Int,
    tabs: List<String>,
    modifier: Modifier = Modifier,
    onTabItemClick: (Int) -> Unit
) {
    TabRow(selectedTabIndex = selectedTabIndex, modifier = modifier) {
        tabs.forEachIndexed { index, _ ->
            Tab(
                selected = index == selectedTabIndex,
                onClick = { onTabItemClick.invoke(index) },
                selectedContentColor = Color.Black,
                text = {
                    Text(
                        text = tabs[index],
                        modifier = Modifier
                    )
                }
            )
        }
    }
}

@Composable
fun CamerasAndDoors(
    viewState: MainViewState,
    viewModel: MainViewModel
) {
    val pullRefreshState = rememberPullRefreshState(
        refreshing = viewState.pullRefreshState,
        onRefresh = viewModel::onRefreshLocalData
    )
    Box(modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 20.dp)
        .pullRefresh(pullRefreshState)) {
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            items(viewState.items.size) {
                val dataItem = viewState.items[it]
                ElevatedCard(
                    colors = CardDefaults.cardColors(
                        containerColor = Color.White
                    ),
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 10.dp
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(vertical = 8.dp)
                ) {
                    Box() {
                        if (!dataItem.snapshot.isNullOrEmpty()) {
                            AsyncImage(
                                model = dataItem.snapshot,
                                contentDescription = null,
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(200.dp),
                            )
                            if (!dataItem.rec.isNullOrEmpty()) {
                                Image(
                                    painter = painterResource(id = R.drawable.ic_2),
                                    contentDescription = null,
                                    modifier = Modifier.align(Alignment.Center)
                                )
                            }
                            if (dataItem.favorites) {
                                Image(
                                    painter = painterResource(id = R.drawable.ic_3),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .align(Alignment.TopEnd)
                                        .padding(5.dp)
                                )
                            }
                        }
                    }
                    Text(
                        text = dataItem.name,
                        modifier = Modifier.padding(10.dp, 20.dp)
                    )
                }
            }
        }
        PullRefreshIndicator(
            refreshing = viewState.pullRefreshState,
            state = pullRefreshState,
            modifier = Modifier.align(Alignment.TopCenter)
        )
    }
}