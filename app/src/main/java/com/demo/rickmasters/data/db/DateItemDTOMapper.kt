package com.demo.rickmasters.data.db

import com.demo.rickmasters.data.remote.DataItem

fun DataItem.toDTO(dataItemType: DataItemDTO.DataType): DataItemDTO = DataItemDTO().let {
    it.id = id
    it.name = name
    it.favorites = favorites
    it.rec = rec
    it.room = room
    it.snapshot = snapshot
    it.dataType = dataItemType.toString()
    it
}