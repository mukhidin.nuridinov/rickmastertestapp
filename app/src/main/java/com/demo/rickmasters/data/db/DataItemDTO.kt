package com.demo.rickmasters.data.db

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

class DataItemDTO(): RealmObject {

    @PrimaryKey
    var _id: ObjectId = ObjectId()
    var id: Int = 0
    var favorites: Boolean = false
    var name: String = ""
    var room: String? = null
    var snapshot: String? = null
    var rec: String? = null
    var dataType: String = DataType.TYPE_CAMERA.toString()

    override fun toString(): String {
        return "id=$id, favorites=$favorites, name=$name, room=$room, snapshot=$snapshot, rec=$rec, dataType=$dataType"
    }

    enum class DataType{
        TYPE_CAMERA,
        TYPE_DOOR
    }
}