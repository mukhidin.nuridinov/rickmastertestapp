package com.demo.rickmasters.data.domain

import com.demo.rickmasters.data.db.DataItemDTO
import com.demo.rickmasters.data.remote.DataItem

fun DataItem.toDomain() = DataItemUi(
    favorites = favorites,
    name = name,
    rec = rec,
    snapshot = snapshot
)

fun DataItemDTO.toDomain() = DataItemUi(
    favorites = favorites,
    name = name,
    rec = rec,
    snapshot = snapshot
)