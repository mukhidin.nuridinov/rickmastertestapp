package com.demo.rickmasters.data.remote

import kotlinx.serialization.Serializable

@Serializable
data class DataItem(
    val id: Int,
    val favorites: Boolean,
    val name: String,
    val room: String? = null,
    val snapshot: String? = null,
    val rec: String? = null
)