package com.demo.rickmasters.data.domain

data class DataItemUi(
    val favorites: Boolean,
    val name: String,
    val snapshot: String?,
    val rec: String?
)