package com.demo.rickmasters.data.remote

import kotlinx.serialization.Serializable

@Serializable
data class CameraData(
    val room: List<String>?,
    val cameras: List<DataItem>
)