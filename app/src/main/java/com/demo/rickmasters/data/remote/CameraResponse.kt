package com.demo.rickmasters.data.remote

import kotlinx.serialization.Serializable

@Serializable
data class CameraResponse(
    val success: Boolean,
    val data: CameraData
)