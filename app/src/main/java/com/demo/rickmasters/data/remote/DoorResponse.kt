package com.demo.rickmasters.data.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DoorResponse(
    val success: Boolean,
    @SerialName("data")
    val data: List<DataItem>
)