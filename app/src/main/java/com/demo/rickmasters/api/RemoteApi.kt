package com.demo.rickmasters.api

import com.demo.rickmasters.data.remote.CameraResponse
import com.demo.rickmasters.data.remote.DoorResponse
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

const val SERVER_URL = "http://cars.cprogroup.ru"
private const val API_CAMERA = "$SERVER_URL/api/rubetek/cameras/"
private const val API_DOOR = "$SERVER_URL/api/rubetek/doors/"

class RemoteApi(private val client: HttpClient) {

    suspend fun getCameras(): CameraResponse = client.get(API_CAMERA).body()

    suspend fun getDoors(): DoorResponse = client.get(API_DOOR).body()

}