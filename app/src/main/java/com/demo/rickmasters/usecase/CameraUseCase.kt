package com.demo.rickmasters.usecase

import com.demo.rickmasters.data.domain.DataItemUi
import com.demo.rickmasters.data.domain.toDomain
import com.demo.rickmasters.repository.CameraAndDoorRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CameraUseCase(
    private val cameraAndDoorRepository: CameraAndDoorRepository
) {
    suspend operator fun invoke(isRefreshLocalData: Boolean): Flow<List<DataItemUi>> = flow {
        emit(cameraAndDoorRepository.getCameras(isRefreshLocalData).map { it.toDomain() })
    }
}