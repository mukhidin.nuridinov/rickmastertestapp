package com.demo.rickmasters.app

import android.app.Application
import com.demo.rickmasters.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin

class RickMastersApp : Application(), KoinComponent {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@RickMastersApp)
            modules(appModule)
        }
    }

}